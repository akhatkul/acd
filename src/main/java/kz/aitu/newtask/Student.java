package kz.aitu.newtask;

public class Student {

    private String name;
    private String lname;
    private String group;
    private double gpa;

    public Student(String name, String lname, String group, double gpa ) {
        this.name = name;
        this.lname = lname;
        this.group = group;
        this.gpa = gpa;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lname;
    }

    public void setLastname(String lname) {
        this.lname = lname;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }


}