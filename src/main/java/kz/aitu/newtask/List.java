package kz.aitu.newtask;

public class List {
    private Student[] students;
    private int sSize = 0;

    private Group[] groups;
    private int gSize = 0;

    public List(){
        students = new Student [5];
        groups = new Group [3];
    }

    public void addStudents(Student student){
        students [sSize++] = student;
    }

    public void addGroups(Group group){
        groups[gSize++] = group;
    }

    public void print(){
        for(int i=0;i<sSize;i++){
            System.out.println(students[i]);
        }
    }

    public double findAverageForCS01() {
            double sum = 0;
            for (int i = 0; i < sSize; i++) {
                if(students[i].getGroup() == "CS-1901") {
                    sum += students[i].getGpa();
                }
            }
            double average = sum / sSize;

            return average;
    }
    public double findAverageForCS02() {
        double sum = 0;
        for (int i = 0; i < sSize; i++) {
            if(students[i].getGroup() == "CS-1902") {
                sum += students[i].getGpa();
            }
        }
        double average = sum / sSize;

        return average;
    }
    public double findAverageForCS03() {
        double sum = 0;
        for (int i = 0; i < sSize; i++) {
            if(students[i].getGroup() == "CS-1903") {
                sum += students[i].getGpa();
            }
        }
        double average = sum / sSize;

        return average;
    }
    public void sort(String type){
        if(type.equals("GPA")){
            GPA();
        }
    }
    public void GPA(){
        System.out.println("sorted:");
        {
            for (int i = 0; i < gSize - 1; i++) {
                int index = i;
                for (int j = i + 1; j < gSize; j++){
                    if (groups[j].getAverage() < groups[index].getAverage()){
                        index = j;
                    }
                }
                Group temp = groups[index];
                groups [index] =(groups[i]);
                groups[i] = temp;
            }
        }
    }

}





