package kz.aitu.newtask;

public class Main {
    public static void main(String[] args){
        List list = new List();

        list.addStudents(new Student("Lilly", "Johnson","CS-1901", 3.05));
        list.addStudents(new Student("Summer", "Andrew", "CS-1901",3.42));
        list.addStudents(new Student("Dee", "Viber", "CS-1901",2.2));
        list.addStudents(new Student("Riley", "Kokorin", "CS-1901", 2.67));
        list.addStudents(new Student("Odonnell", "Mamaev", "CS-1901", 3));
        list.addStudents(new Student("Jayne", "Arshavin", "CS-1902",4));
        list.addStudents(new Student("Jennings", "Konoplyanka", "CS-1902", 3.05));
        list.addStudents(new Student("Barton", "Dibala", "CS-1902",3.33));
        list.addStudents(new Student("Aguirre", "Kun", "CS-1902",1.33));
        list.addStudents(new Student("Vickie", "Jim", "CS-1902",3.67));
        list.addStudents(new Student("Hazel", "Malakhov", "CS-1903", 3));
        list.addStudents(new Student("Dalton", "Belov", "CS-1903",3));
        list.addStudents(new Student("Burke", "Smirnov", "CS-1903",2.33));
        list.addStudents(new Student("Small", "Magazin", "CS-1903",3.67));
        list.addStudents(new Student("May", "Laskovyi", "CS-1903",2.12));

        list.addGroups(new Group("CS-1901", list.findAverageForCS01()));
        list.addGroups(new Group("CS-1902", list.findAverageForCS02()));
        list.addGroups(new Group("CS-1903", list.findAverageForCS03()));


        list.sort("GPA");
        list.print();
    }

}
