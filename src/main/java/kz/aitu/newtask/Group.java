package kz.aitu.newtask;

public class Group {
    private String name;
    public double average;

    public Group(String name, double average){
        this.name = name;
        this.average = average;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

