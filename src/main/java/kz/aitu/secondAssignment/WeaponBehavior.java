package kz.aitu.secondAssignment;

public interface WeaponBehavior {
    void useWeapon();
}
