package kz.aitu.secondAssignment;

public class Knight extends Character {
    @Override
    public void fight() {
        System.out.println("Knight is here");
        this.weapon.useWeapon();
    }

    public Knight() {
        weapon = new SwordBehavior();
    }

}
