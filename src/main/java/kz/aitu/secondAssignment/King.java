package kz.aitu.secondAssignment;

import kz.aitu.secondAssignment.AxeBehavior;
import kz.aitu.secondAssignment.Character;

public class King extends Character {
    public King() {
        weapon = new AxeBehavior();
    }

    @Override
    public void fight() {
        System.out.println("King is here");
        this.weapon.useWeapon();
    }

}
