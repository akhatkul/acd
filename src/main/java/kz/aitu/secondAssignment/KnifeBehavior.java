package kz.aitu.secondAssignment;

public class KnifeBehavior implements WeaponBehavior {
    @Override
    public void useWeapon() {
        System.out.println("My weapon is knife");
    }

}
