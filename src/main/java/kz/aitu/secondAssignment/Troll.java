package kz.aitu.secondAssignment;

import kz.aitu.secondAssignment.Character;
import kz.aitu.secondAssignment.SwordBehavior;

public class Troll extends Character {
    public Troll() {
        weapon = new SwordBehavior();
    }

    @Override
    public void fight() {
        System.out.println("Troll is here");
        this.weapon.useWeapon();
    }

}
