package kz.aitu.secondAssignment;

import kz.aitu.secondAssignment.BowAndArrowBehavior;
import kz.aitu.secondAssignment.Character;

public class Queen extends Character {
    public Queen() {
        weapon = new BowAndArrowBehavior();
    }

    @Override
    public void fight() {
        System.out.println("Queen is here");
        this.weapon.useWeapon();
    }

}
