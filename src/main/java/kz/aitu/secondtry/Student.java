package kz.aitu.secondtry;

public class Student {

    private String name;
    private String lname;
    private String mail;

    public Student(String name, String lname, String mail) {
        this.name = name;
        this.lname = lname;
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lname;
    }

    public void setLastname(String lname) {
        this.lname = lname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

}
