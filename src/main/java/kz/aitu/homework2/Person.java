package kz.aitu.homework2;

public class Person {
    private String name;
    private int birthYear;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public Person() {
    }

    public Person(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    void age(){
        int ageNumber = 2020 - birthYear;
        System.out.println(ageNumber);
    }

    void input(String name, int birthYear){
        setName(name);
        setBirthYear(birthYear);
    }

    void output(){
        System.out.println("Name is " + name + ", birth year is " +birthYear);
    }

    void changeName(String newName){
        name = newName;
    }
}
