package kz.aitu.homework2;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person();
        Person person2 = new Person();
        Person person3 = new Person();
        Person person4 = new Person();

        person1.input("John", 1951);
        person2.input("Brian", 1947);
        person3.input("Freddie", 1946);
        person4.input("Roger", 1949);


        System.out.println("Person 1 : " + person1.getName() + ", " + person1.getBirthYear());
        System.out.println("Person 2 : " + person2.getName() + ", " + person2.getBirthYear());
        System.out.println("Person 3 : " + person3.getName() + ", " + person3.getBirthYear());
        System.out.println("Person 4 : " + person4.getName() + ", " + person4.getBirthYear());

        person1.age();
        person2.age();
        person3.age();
        person4.age();

        person1.changeName("Angus");
        person2.changeName("Chris");
        person3.changeName("Steve");
        person4.changeName("Axl");

        System.out.println("Person 1 : " + person1.getName() + ", " + person1.getBirthYear());
        System.out.println("Person 2 : " + person2.getName() + ", " + person2.getBirthYear());
        System.out.println("Person 3 : " + person3.getName() + ", " + person3.getBirthYear());
        System.out.println("Person 4 : " + person4.getName() + ", " + person4.getBirthYear());
    }

}
