package kz.aitu.another;

public class List {

    private Student[] array;
    private int size = 0;

    public List() {
        array = new Student[15];
    }

    public void add(Student student) {
        array[size++] = student;
    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.println(array[i]);
        }

    }

    public void sort(String type) {
        if(type.equals("mail")) {
            mail();
        }
    }

    public void mail() {
        System.out.println("sorted:");
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (array[j].getMail().codePointAt(0) < array[index].getMail().codePointAt(0)){
                    index = j;
                }
            }

            Student temp = array[index];
            array[index] =(array[i]);
            array[i] = temp;
        }
    }
}
