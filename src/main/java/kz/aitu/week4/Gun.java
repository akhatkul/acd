package kz.aitu.week4;

public class Gun {
    private String name;
    private boolean isMagazineConnected;
    private boolean isPredohranitelOff;
    private boolean isPatronVPatronnike;

    public Gun(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void connectMagazine(){
        isMagazineConnected = true;
        System.out.println("Your magazine is connected");
    }

    public void switchOffPredohranitel(){
        isPredohranitelOff = true;
        System.out.println("Your fuse is turned off");
    }

    public void dernutZatvor(){
        if(isPredohranitelOff){
            isPatronVPatronnike = true;
            System.out.println("Your Breechblock distorted");
        }else {
            System.out.println("Check your fuse");
        }
    }

    public void shoot() throws GunIsNotReadyException {
        if(isPatronVPatronnike && isPredohranitelOff && isMagazineConnected){
            System.out.println("Your " + name + " is ready to shoot");
        } else {
            throw new GunIsNotReadyException("Your " + name + " is not ready to shoot, check your gun");
        }
    }

}
