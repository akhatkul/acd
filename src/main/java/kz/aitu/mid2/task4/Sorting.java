package kz.aitu.mid2.task4;

public class Sorting{
    public char[] sort(String a){
        char arr[];
        arr = new char[a.length()];
        for (int i=0;i<a.length();i++){
            arr[i] = a.charAt(i);

        }
        sorting(arr ,0,a.length()-1);
        return arr;
    }

    private void sorting(char arr[], int low, int high){
        if (low<high){
            int b = sorted(arr, low, high);
            sorting(arr,low,b-1);
            sorting(arr,b+1,high);
        }
    }

    private int sorted(char arr[], int low, int high){
        char temp;
        char pivot = arr[high];
        int a = low - 1;
        for (int i=low;i<high;i++){
            if (arr[i]<pivot){
                a++;
                temp = arr[i];
                arr[i] = arr[a];
                arr[a] = temp;
            }

        }
        temp = arr[a+1];
        arr[a+1] = pivot;
        arr[high] = temp;
        return a+1;
    }
}
