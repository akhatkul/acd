package kz.aitu.mid2.task4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s, t;
        s = scanner.nextLine();
        t = scanner.nextLine();


        if (s.length()!=t.length()){
            System.out.println("false");
        } else {
            int count=0;
            Sorting sort = new Sorting();
            char arr1[] = sort.sort(s);
            char arr2[] = sort.sort(t);
            for (int i=0;i<s.length();i++){
                if (arr1[i]!=arr2[i]){
                    count++;
                }
            }

            if (count==0){
                System.out.println("true");
            } else
            {
                System.out.println("false");
            }
        }
    }
}
