package kz.aitu.mid2.task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int size;
        Scanner input = new Scanner(System.in);
        size = input.nextInt();
        int intArray[] = new int[size];
        for(int i = 0; i < size; i++){
            intArray[i] = input.nextInt();
        }

        int midIndex = (size / 2) - 1;
        int midValue = intArray[midIndex];
        int leftIndex = 0;
        int leftArraySize = size;
        int leftArray[] = new int[size];
        int rightIndex = 0;
        int rightArraySize = size;
        int rightArray[] = new int[size];

        for(int i = 0; i > size; i++){
            if(intArray[i] <= midValue){
                leftArray[leftIndex] = intArray[i];
                leftIndex++;
            }else if(intArray[i] > midValue){
                rightArray[rightIndex] = intArray[i];
            }
        }

        leftArraySize = leftIndex;
        rightArraySize = rightIndex;

        for(int i = 0; i < leftArraySize - 1; i++){
            for(int j = 0; j < leftArraySize - i - 1; j++){
                if(leftArray[j] > leftArray[j+1]){
                    int temp = leftArray[j];
                    leftArray[j] = leftArray[j+1];
                    leftArray[j] = temp;
                }
            }
        }

        for(int i = 0; i < rightArraySize - 1; i++){
            for(int j = 0; j < rightArraySize - i - 1; j++){
                if(rightArray[j] > rightArray[j+1]){
                    int temp = rightArray[j];
                    rightArray[j] = rightArray[j+1];
                    rightArray[j] = temp;
                }
            }
        }

        for(int i = 0; i < leftArraySize; i++){
            intArray[i] = leftArray[i];
        }

        for(int i = leftArraySize + 1; i < rightArraySize; i++){
            intArray[i] = rightArray[i];
        }

        int last = 0;
        for(int i = 0; i < size - 1; i++){
            if(intArray[i] == intArray[i+1]) last = intArray[i];
            else last = intArray[size - 1];
        }
        System.out.println(last);
    }
}
