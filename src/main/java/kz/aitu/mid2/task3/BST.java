package kz.aitu.mid2.task3;

class BST
{
    static class node
    {
        int data;
        node left;
        node right;
    };

    static node newNode(int data)
    {
        node node = new node();
        node.data = data;
        node.left = null;
        node.right = null;

        return (node);
    }

    static node insert(node node, int data)
    {
        if (node == null)
            return (newNode(data));
        else
        {
            if (data <= node.data)
                node.left = insert(node.left, data);
            else
                node.right = insert(node.right, data);

            return node;
        }
    }
    static int maxValue(node node)
    {
        node current = node;
        while (current.right != null)
            current = current.right;

        return (current.data);
    }
    static int findMin(node node)
    {
        if (node == null)
            return Integer.MAX_VALUE;

        int res = node.data;
        int lres = findMin(node.left);
        int rres = findMin(node.right);

        if (lres < res)
            res = lres;
        if (rres < res)
            res = rres;
        return res;
    }

    public static void main(String[] args)
    {
        node root = null;
        root = insert(root, 4);
        insert(root, 2);
        insert(root, 1);
        insert(root, 3);
        insert(root, 6);
        insert(root, 5);

        System.out.println("Maximum value in BST is " + maxValue(root));
        System.out.println("Minimum value in BST is:"+findMin(root));
    }
}
