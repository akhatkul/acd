package kz.aitu.students;

public class List {
    private Student[] studentList;
    private int size = 0;

    public List() {
        studentList = new Student[15];
    }

    public void add(Student student) {
        studentList[size++] = student;
    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.println(studentList[i]);
        }

    }

    public void sort(String type) {
        if (type.equals("mail")) {
            mail();
        }
    }

    public void mail() {
        System.out.println("mail: ");
        char [] array = new char[0];
        quickSortStart(array);
    }

    private void quickSortStart(char[] array) {
        int size = array.length - 1;
        quickSort(array, 0, size);
    }

    private static void quickSort(char[] array, int first, int last){
        if(first < last){
            int j = first - 1;

            for(int i = first; i < last; i++){
                if(array[i] < array[last]){
                    j = j + 1;
                    char temp = array[j];
                    array[j] = array[i];
                    array[i] = temp;
                }
            }

            j = j + 1;
            char temp = array[j];
            array[j] = array[last];
            array[last] = temp;

            quickSort(array, first, j-1);
            quickSort(array, j+1, last);
        }
    }
}

