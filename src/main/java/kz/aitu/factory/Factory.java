package kz.aitu.factory;

public class Factory {
    public static Mobile createMobile(String type){
        if(type.equals(Mobile.Apple)){
            return new Apple(4, "A13", "Apple GPU");
        }
        else if(type.equals(Mobile.SAMSUNG)){
            return new Samsung(12,"Snapdragon 865", "Adreno 650");
        }
        else if(type.equals(Mobile.Google)){
            return new Google(6,"Snapdragon 855", "Adreno 640");
        }
        else if(type.equals(Mobile.Xiaomi)){
            return new Xiaomi(6,"Snapdragon 720", "Adreno 618");
        }
        else if(type.equals(Mobile.Huawei)){
            return new Huawei(8,"Kirin 990", "Mali-G76 MP16");
        }
        else if(type.equals(Mobile.Oppo)){
            return new Oppo(12,"Snapdragon 765", "Adreno 620");
        }
        else{
            return null;
        }
    }
}
