package kz.aitu.factory;

public class Main {
    public static void main(String[] args)
    {
        Mobile mobile = Factory.createMobile(Mobile.SAMSUNG);
        Mobile mobileOne = Factory.createMobile(Mobile.Apple);
        Mobile mobileTwo = Factory.createMobile(Mobile.Google);
        Mobile mobileThree = Factory.createMobile(Mobile.Huawei);
    }
}
