package kz.aitu.factory;

    public interface Mobile {
    public static final String Apple = "iphone";
    public static final String SAMSUNG = "samsung";
    public static final String Google = "google";
    public static final String Huawei = "huawei";
    public static final String Xiaomi = "xiaomi";
    public static final String Oppo = "oppo";
}
