package kz.aitu.BST;

public class Node {
    private Integer key;
    private String value;
    private kz.aitu.week6.Node left;
    private kz.aitu.week6.Node right;

    public Node(Integer key, String value) {
        this.key = key;
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public kz.aitu.week6.Node getLeft() {
        return left;
    }

    public void setLeft(kz.aitu.week6.Node left) {
        this.left = left;
    }

    public kz.aitu.week6.Node getRight() {
        return right;
    }

    public void setRight(kz.aitu.week6.Node right) {
        this.right = right;
    }
}
