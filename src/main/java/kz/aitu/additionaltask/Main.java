package kz.aitu.additionaltask;

public class Main {
    public static void main(String[] args) {
        List list = new List();

        list.add(new Student("Lilly", "Johnson", "ljohnson@gmail.com"));
        list.add(new Student("Summer", "Andrew", "ssandrreww22@gmail.com"));
        list.add(new Student("Dee", "Viber", "viberdee@gmail.com"));
        list.add(new Student("Riley", "Kokorin", "kokokorinr@gmail.com"));
        list.add(new Student("Odonnell", "Mamaev", "odonnellm@gmail.com"));
        list.add(new Student("Jayne", "Arshavin", "jarshavin003@gmail.com"));
        list.add(new Student("Jennings", "Konoplyanka", "coolguy228@gmail.com"));
        list.add(new Student("Barton", "Dibala", "qwerty12345@gmail.com"));
        list.add(new Student("Aguirre", "Kun", "akun11@gmail.com"));
        list.add(new Student("Vickie", "Jim", "vickiejim@gmail.com"));
        list.add(new Student("Hazel", "Malakhov", "hmalakhov@gmail.com"));
        list.add(new Student("Dalton", "Belov", "belovd@gmail.com"));
        list.add(new Student("Burke", "Smirnov", "barsmirnov@gmail.com"));
        list.add(new Student("Small", "Magazin", "bolshieskidki@gmail.com"));
        list.add(new Student("May", "Laskovyi", "laskovyimay05@gmail.com"));

        list.sort("name");
        list.print();

        list.sort("lname");
        list.print();

        list.sort("mail");
        list.print();
    }

}
