package kz.aitu.homework;

public class Main {
    public static void main(String[] args) {
        Employee employee_1 = new Employee();
        Employee employee_2 = new Employee();
        Employee employee_3 = new Employee();

        employee_1.setRate(15);
        employee_1.setHours(10);
        employee_1.setName("John");

        employee_2.setRate(18);
        employee_2.setHours(12);
        employee_2.setName("Axl");

        employee_3.setRate(30);
        employee_3.setHours(13);
        employee_3.setName("Freddie");

        employee_1.salary("John");
        employee_1.toString("John");
        employee_1.changeRate("John", 20);
        employee_1.bonuses("John");

        System.out.println("First employee's name is " + employee_1.getName());
        employee_1.salary();
        employee_1.toString();
        employee_1.changeRate(25);
        employee_1.bonuses();

        System.out.println("Second employee's name is " + employee_2.getName());
        employee_2.salary();
        employee_2.toString();
        employee_2.changeRate(40);
        employee_2.bonuses();

        System.out.println("Third employee's name is ");
        employee_3.salary();
        employee_3.toString();
        employee_3.changeRate(37);
        employee_3.bonuses();

        System.out.println("Hours of all employees");
        System.out.println("Hours of first employee is");
        System.out.println(employee_1.getName() + " - " + employee_1.getHours());

        System.out.println("Hours of second employee is");
        System.out.println(employee_2.getName() + " - " + employee_2.getHours());

        System.out.println("Hours of third employee is");
        System.out.println(employee_3.getName() + " - " + employee_3.getHours());
    }

}
