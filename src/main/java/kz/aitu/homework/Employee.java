package kz.aitu.homework;

public class Employee {
    private String name;
    private int rate;
    private int hours;

    static int totalSum;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public static int getTotalSum() {
        return totalSum;
    }

    public static void setTotalSum(int totalSum) {
        Employee.totalSum = totalSum;
    }

    public Employee() {
    }

    public Employee(String name, int rate) {
        this.name = name;
        this.rate = rate;
    }

    public Employee(String name, int rate, int hours) {
        this.name = name;
        this.rate = rate;
        this.hours = hours;
    }

    void salary(String name){
        int salaryAmount = rate * hours;
        System.out.println(salaryAmount);
    }

    void salary(){
        int salaryAmount = rate * hours;
        System.out.println("Salary is : " + salaryAmount);
    }

    void toString(String employeeName){
        String aboutEmployee = employeeName + " " + rate + " " + hours;
        System.out.println(aboutEmployee);
    }

    void changeRate(String name, int newRate){
        rate = newRate;
        System.out.println(rate);
    }

    void changeRate(int newRate){
        rate = newRate;
        System.out.println("New rate is : " + rate);
    }

    void bonuses(String name){
        int bonus = rate * hours / 10;
        System.out.println(bonus);
    }

    void bonuses(){
        int bonus = rate * hours / 10;
        System.out.println("Bonus is : " + bonus);
    }
}
