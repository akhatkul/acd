package kz.aitu.weekthree;

public class StaticsDisplay implements Observer, DislpayElement{
    private float temperature;
    private float humidity;
    private float pressure;
    private Subject weatherData;
    int counter = 0;
    static float sum_temp = 0;
    static float max_temp = -460;
    static float min_temp = 212;


    public StaticsDisplay(Subject weatherData){
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }


    @Override
    public void update(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        if(this.temperature > max_temp){
            max_temp = this.temperature;
        }
        if(this.temperature < min_temp){
            min_temp = this.temperature;
        }
        counter++;
        sum_temp = sum_temp + this.temperature;
        display();
    }


    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + sum_temp/counter
                + "/" + max_temp + "/" + min_temp);


    }

}
