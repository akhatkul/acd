package kz.aitu.week6;

public class BSTree {
    public Node root;

    public BSTree() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        Node tempNode = new Node(key, value);
        if(root == null){
            root = tempNode;
            return;
        }else{
            Node tempNodeTwo = root;
            Node parent = null;

            while(true){
                parent = tempNodeTwo;
                if(key < tempNodeTwo.getKey()){
                    tempNodeTwo = tempNodeTwo.getLeft();
                    if(tempNodeTwo == null){
                        parent.setLeft(tempNode);
                        return;
                    }
                }else{
                    tempNodeTwo = tempNodeTwo.getRight();

                    if(tempNodeTwo == null){
                        parent.setRight(tempNode);
                        return;
                    }
                }
            }
        }
    }

    public boolean delete(int key){
        Node parent = root;
        Node currentNode = root;
        boolean left = false;

        while(currentNode.getKey() != key){
            parent = currentNode;
            if(key < currentNode.getKey()){
                currentNode = currentNode.getLeft();
                left = true;
            }else{
                currentNode = currentNode.getRight();
                left = false;
            }

            if(currentNode == null) return false;
        }

        if(currentNode.getLeft() == null && currentNode.getRight() == null){
            if(currentNode == root){
                root = null;
            }

            if(left){
                parent.setLeft(null);
            }else parent.setRight(null);
        }else if(currentNode.getLeft() == null){
            if(currentNode == root) root = currentNode.getRight();
            else if(left) parent.setLeft(currentNode.getRight());
            else parent.setRight(currentNode.getRight());
        }else if(currentNode.getRight() == null){
            if(currentNode == root) root = currentNode.getLeft();
            else if(left) parent.setLeft(currentNode.getLeft());
            else parent.setRight(currentNode.getLeft());
        }else if(currentNode.getLeft() != null && currentNode.getRight() != null){
            Node replacement = getMovedNode(currentNode);
            if(currentNode == root){
                root = replacement;
            }else if(left) parent.setLeft(replacement);
            else{
                parent.setRight(replacement);
            }

            replacement.setRight(currentNode.getRight());
        }

        return true;
    }



    public Node getMovedNode(Node movedNode){
        Node parent = null;
        Node movement = null;
        Node currentNode = movedNode.getLeft();

        while(currentNode != null){
            parent = movement;
            movement = currentNode;
            currentNode = currentNode.getRight();
        }

        if(movement != movedNode.getRight()){
            parent.setRight(movement.getLeft());
            movement.setLeft(movedNode.getLeft());
        }

        return movement;
    }

    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public String findWithoutRecursion(Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending(){
        printNodeAscending(root);
    }

    private void printNodeAscending(Node root) {
        if(root == null) return;
        printNodeAscending(root.getLeft());
        System.out.print(root.getValue() + " ");
        printNodeAscending(root.getRight());
    }

    public void printAll() {
        for (int i=1; i<=size(root); i++) {
            printNode(root, i);
        }
    }

    int size(Node node){
        if (node == null) return 0;
        else return(size(node.getLeft()) + 1 + size(node.getRight()));
    }

    private void printNode(Node root, int r) {
        if (root == null) return;
        if (r == 1) System.out.print(root.getValue() + " ");
        else if (r > 1) {
            printNode(root.getLeft(),r-1);
            printNode(root.getRight(),r-1);
        }
    }
}

